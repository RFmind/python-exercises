

def create_tree(max_length):
    tree = []
    start = 1

    if max_length % 2 == 0:
        start+=1

    for x in range(start, max_length+1, 2):

        nummer_spaces= max_length-x
        line = nummer_spaces/2 * ' ' + x * '*'  + nummer_spaces/2 * ' ' # build a line

        tree.append(line) # add line to list

    return tree




#####################################################
# some tests :)
#####################################################

print("tree = " + str(create_tree(3)) + "\n")

print("passed" if len(create_tree(3)[0]) == 3 else "failed")
print("passed" if len(create_tree(3)[1]) == 3 else "failed")

print("passed" if create_tree(3) == [" * ", "***"] else "failed")
print("passed" if create_tree(5) == ["  *  ", " *** ", "*****"] else "failed")
print("passed" if create_tree(4) == [" ** ", "****"] else "failed")
print("passed" if create_tree(6) == ["  **  ", " **** ", "******"] else "failed")

print("")
for line in create_tree(7):
    print(line)
