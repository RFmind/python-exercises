

def count_number_of_occurences(inputstring, word):
    """
    given a string with words and a word,
    return the number of times the word occurs in the string
    """
    counter = 0
    newstr= inputstring.split(' ')

    for x in newstr:
        if x ==  word:
            counter+=1

    return counter




##################################################"
# Some tests :)
##################################################

print("passed" if count_number_of_occurences("a b c", "a") == 1 else "failed")
print("passed" if count_number_of_occurences("a a c", "a") == 2 else "failed")
print("passed" if count_number_of_occurences("some other word", "some") == 1 else "failed")
