from unittest import TestCase, main
from solution import is_valid_password

class ValidatorTest(TestCase):

    def test_password_has_no_digits(self):
        self.assertEqual(False, is_valid_password('abcDEF'))

    def test_password_has_no_uppercase_leters(self):
        self.assertEqual(False, is_valid_password('abc123'))

    def test_password_has_no_lowercase_letters(self):
        self.assertEqual(False, is_valid_password('ABC123'))

    def test_password_is_not_long_enough(self):
        self.assertEqual(False, is_valid_password('Abc12'))

    def test_password_is_valid(self):
        self.assertEqual(True, is_valid_password('AbC123'))
        self.assertEqual(True, is_valid_password('AAAbBBee3lj4'))

if __name__ == '__main__':
    main()
