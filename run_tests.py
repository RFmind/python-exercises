from unittest import main

from digits.test import DigitsTest
from validation.test import ValidatorTest
from eightqueens.test import EightQueensTest
from gameoflife.test import GameOfLifeTest
from conwaysequence.test import ConwaySequenceTest

if __name__ == '__main__':
    main()
