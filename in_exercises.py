

def collection_has(coll, item):
    """
    given a collection and an item,
    return True if item is present in the collection,
    else return False.
    """
    for x in coll:
        if x == item:
            return True
    return False
        

def how_many(colls, item):
    """
    given a list of lists that contain strings.
    given also a string,
    return how many lists contain the given string.
    """
    
    counter=0
    for x in colls:
        for y in x:
            if y == item:
                counter += 1
    return counter
    

def has_max(coll, item, max):
    """
    given a list of strings, a string and an int.
    return if the item is 'max' times present in the coll.

    for example: has_max(['a', 'a'], 'a', 2) => True

    because the coll ['a', 'a'] has 2 times the item 'a', the
    function returns True.
    """
    
    
    return collection_has(coll, item) and how_many(coll, item) == max
        
    
    
    
    
    
    
    





###################################################################
# Some tests :)
###################################################################

from unittest import TestCase
from unittest import main

class TestThisFile(TestCase):

    def test_collection_has(self):
        self.assertEqual(collection_has([1,2], 1), True)
        self.assertEqual(collection_has(["1", "2"], "1"), True)
        self.assertEqual(collection_has(["1"], "2"), False)

    def test_how_many(self):
        self.assertEqual(how_many([["a"], ["a"]], "a"), 2)
        self.assertEqual(how_many([["a"], ["b"]], "a"), 1)

    def test_has_max(self):
        self.assertEqual(has_max(["1","2","2"], "2", 2), True)
        self.assertEqual(has_max(["a","a"], "a", 3), False)

if __name__ == '__main__':
    main()
