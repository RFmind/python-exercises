
def ends_with(inputstring, extension):
    """
    given a string,
    return True if it ends on the given extension,
    otherwise False

    Tip: a string is actually like a list!
         you can select element like a list:
             somestring[0]
             somestring[0:5]
             somestring[2:]
             ...

         you can also iterate like through a list:
             for i in somestring:
                 pass
    """



    extensionLen = len(extension)
    stringLengthNoExtension = len(inputstring) - len(extension)

    if inputstring[stringLengthNoExtension: len(inputstring)] == extension:
        return True
    else:
        return False




def count_words(inputstring):
    """
    given a string,
    split the string by space,
    return the number of words
    """

    newstr= inputstring.split(' ')
    return len(newstr)




#####################################################
# Some tests :)
#####################################################

print("passed" if ends_with("haha", "ha") else "failed")
print("passed" if ends_with("banana", "nana") else "failed")

print("passed" if count_words("hello there") == 2 else "failed")
print("passed" if count_words("I am evil") == 3 else "failed")
