
def list_of_lists(nested_list):
    """
    given a 2D list containing ints.
    return a 2D list containing True or False.
    True if the value of the cell is bigger than 3. otherwise False.
    """

    pass


####################################################################
# Some tests
####################################################################

from unittest import (TestCase, main)

class TestThisFile(TestCase):

    def test_list_of_lists(self):
        testdata = [
            [1, 2, 3],
            [3, 2, 3]
        ]

        expected = [
            [False, False, True],
            [True, False, True]
        ]

        self.assertEqual(list_of_lists(testdata), expected)

if __name__ == '__main__':
    main()
