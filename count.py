
################################################################

def count_chars(inputstring):
    """Return the number of characters in the inputstring"""
    return len(inputstring)


################################################################

def count_elements(inputlist):
    """Return the number of elements in the inputlist"""

    """
    counter = 0
    for x in inputlist:
        counter += 1

    return counter
    """

    return len(inputlist)

################################################################

def count_nested(inputdict):
    """Input: a dict in the form {'a': ['b']}.
       return a dict that maps keys from inputdict to the
       the number of values for this key.
       in this example the output should be {'a': 1}"""

    diccionary = {}

    for x in inputdict.keys():
        val = inputdict[x]
        diccionary[x] = len(val)

    return diccionary

################################################################


print("passed" if count_chars("abc") == 3 else "failed")
print("passed" if count_chars("abcd") == 4 else "failed")
print("passed" if count_chars("abc d") == 5 else "failed")

print("passed" if count_elements([1,2,3]) == 3 else "failed")
print("passed" if count_elements([4,2]) == 2 else "failed")

print("passed" if count_nested({"a": ["b"]}) == {"a": 1} else "failed")
