
def conway_next(inputstring):
    """
    given an inputstring,
    return a list containing tuples which contain the count of each
    character and the character.

    for example:

        conway_next("aabb") == [(2, "a"), (2, "b")]
        conway_next("abbb") == [(1, "a"), (3, "b")]
    """
    result=[]
    anterior= inputstring [0]
    counter = 0

    for x in inputstring:
        if x != anterior:
            result.append((counter,anterior))
            anterior= x
            counter =1
        else:
            counter +=1
    result.append((counter,x))
    return result

def conway_into_string(inputlist):
    """
    given a list that is produced by the function above.
    return a string that is a concatination of everything in the list

    [(2, "a"), (2, "b")]
    """
    result= ""
    for x in inputlist:
        result += str(x[0])
        result += str(x[1])
    return result



##########################################################
# Some tests :)
##########################################################

from unittest import TestCase
from unittest import main

class TestThisFile(TestCase):

    def test_conway_next(self):
        self.assertEqual(conway_next("aabbccc"), [(2, "a"), (2, "b"), (3, "c")])
        self.assertEqual(conway_next("abbb"), [(1, "a"), (3, "b")])

    def test_conway_into_string(self):
        self.assertEqual(conway_into_string(conway_next("aabb")), "2a2b")

if __name__ == '__main__':
    main()
