from functools import reduce
from operator import mul


def count_sum_prod(inputlist):
    """
    given an inputlist containing lists of numbers,
    return a list containing lists of the count, sum and product
    of the numbers.

    example: [[1,2,3], [1,1,1]] => [[3,6,6], [3,3,1]]
    """

    '''
    
    result2=[]
    
    for x in inputlist:
        
        result = []
        nummer_elements= len(x)
        summ_elements=0
        mult_elements=1
        
        for y in x:
            
            
            summ_elements += y
            mult_elements  *= y

        result.append(nummer_elements)
        result.append(summ_elements)
        result.append(mult_elements)
    
        result2.append(result)
    
    return result2

'''

    '''
    result=[]
    
    for x in inputlist:
        
        
        result.append([len(x), sum(x), reduce(mul, x , 1) ])
        
    # return result
     '''
    return [[len(x), sum(x), reduce(mul, x, 1)] for x in inputlist]
    
    
    





###################################################
# Some tests :)
###################################################

tests = [{'input': [[1,2,3], [1,1,1]],
          'output': [[3,6,6], [3,3,1]]}]

print("passed" if count_sum_prod(tests[0]['input']) == tests[0]['output'] else "failed")
