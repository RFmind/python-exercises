# Python Exercises

Small kata-like exercises for teaching the basics of python.

## Execute

You can run each file separately using the python interpreter. `python filename.py`

Tests are usually defined in each file locally.
