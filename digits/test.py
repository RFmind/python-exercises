from unittest import TestCase, main
from solution import *

class DigitsTest(TestCase):

    def test_string_has_digit(self):
        self.assertFalse(string_has_digit('abc', 2))
        self.assertTrue(string_has_digit('abc2', 2))
        self.assertFalse(string_has_digit('abc3', 2))
        self.assertTrue(string_has_digit('ab4c', 4))

    def test_times_digit_in_string(self):
        self.assertEqual(1, times_digit_in_string('abc2', 2))
        self.assertEqual(2, times_digit_in_string('a1b1c', 1))

    def test_sum_of_all_digits_in_string(self):
        self.assertEqual(3, sum_of_all_digits_in_string('1a2'))
        self.assertEqual(4, sum_of_all_digits_in_string('1e11n1'))

    def test_most_frequent_digit(self):
        self.assertEqual(1, most_frequent_digit('112er'))
        self.assertEqual(2, most_frequent_digit('33ff22'))


if __name__ == '__main__':
    main()