def string_has_digit(s, digit):

	for i in s:
		try:
			if int(i) == digit:
				return True
		except:
			continue
	return False

def times_digit_in_string(s, digit):
    
	count = 0

	for i in s:
		try:
			if int(i) == digit:
				count = count +1
		except:
			continue
	return count


def sum_of_all_digits_in_string(s):
    
    result = 0

    for i in s:
        try:
            digit = int(i)
            result = result + digit
        except:
            continue

    return result

def most_frequent_digit(s):
    
    count = {}
    maxim = [-1,-1]

    for i in s:

        try:
            digit = int(i)
        except:
            continue

        if digit in count:
            count[digit] = count[digit] + 1
        else:
            count[digit] = 1

        # alternative code: count[digit] = count[digit]+1 if digit in count else 1
        if count[digit] > maxim[1]:
            maxim[0] = digit
            maxim[1] = count[digit]
            # alternative for 49 & 50 --> maxim = (digit, count[digit])
        elif count[digit] == maxim[1]:
            if digit < maxim[0]:
                maxim[0] = digit

    return maxim[0]

