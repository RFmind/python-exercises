from unittest import TestCase, main
from solution import (
    string_into_counts,
    counts_into_string,
    conway_seq_for,
    times_until_seq_is_found
)

class ConwaySequenceTest(TestCase):

    def test_string_into_counts(self):
        self.assertEqual(
            [(2, 'a'), (1, 'b')],
            string_into_counts('aab'))
        self.assertEqual(
            [(1, 'c'), (2, 'b'), (1, 'c')],
            string_into_counts('cbbc'))

    def test_counts_into_string(self):
        self.assertEqual(
            '1a1b',
            counts_into_string([(1,'a'), (1, 'b')]))
        self.assertEqual(
            '1e2g1e',
            counts_into_string([(1,'e'), (2,'g'), (1,'e')]))

    def test_conway_seq_for(self):
        starting_seq = 'abc'
        gen1 = '1a1b1c'
        gen2 = '111a111b111c'
        gen3 = '311a311b311c'

        self.assertEqual(gen1, conway_seq_for(starting_seq, 1))
        self.assertEqual(gen2, conway_seq_for(starting_seq, 2))
        self.assertEqual(gen3, conway_seq_for(starting_seq, 3))

    def test_times_until_seq_is_found(self):
        pass

if __name__ == '__main__':
    main()