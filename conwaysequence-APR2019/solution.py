
def string_into_counts(inputstring):
    result = []

    previous_letter = inputstring[0]
    count = 0
    
    for letter in inputstring:
        if letter == previous_letter:
            count += 1
        else:
            result.append((count,previous_letter))
            previous_letter = letter
            count = 1
    result.append((count,previous_letter))

    return result

def counts_into_string(inputlist):
    result = ""

    for item in inputlist:
        item2 = str(item[0])
        result = result + item2 + item[1]

    return result

def conway_seq_for(inputstring, n):
    
    count = 0
    current_string = inputstring
    while count < n:
        counts = string_into_counts(current_string)
        current_string = counts_into_string(counts)
        count += 1

    return current_string

def times_until_seq_is_found(starting_string, target_string):
    pass
