
def reverse_a_word(word):
    """
    given a string,
    return the string reversed.
    an example:

        reverse_a_word("haha") => "ahah"
        reverse_a_word("hello") => "olleh"

    remember you can work with strings like with lists..
    """
    return word[::-1]

################################################################

def translate_a_sentence(sentence, encoding):
    """
    given a sentence (string) and
          an encoding
              (a dict which maps each letter in the alphabet
              to a translated version)
    return the translated sentence as a single string
    """
    
    result = ""
    
    for char in sentence:
        #char=x
        if char in encoding.keys():
            #char = encoding[x]
            result += encoding[char]
        else:
            #if char == " ":
            #    result += " "
            result += char    
            
    return result
    
        
        
    '''  
        for k in encoding:
            if charac == k:
                for l in encoding:
                    charac=l
                    result+= str(charac)
    return result
                    
    
    '''
    





################################################################
# Some tests :)
################################################################

from string import ascii_lowercase as letters
from unittest import (TestCase, main)

class TestStringExercises(TestCase):

    def test_reverse_a_word(self):
        self.assertEqual(reverse_a_word("haha"), "ahah")
        self.assertEqual(reverse_a_word("hello"), "olleh")
        self.assertEqual(reverse_a_word("some"), "emos")

    def test_translate_a_sentence(self):
        test_encoding = {
            letter: str(num) for num, letter in enumerate(letters)
        }

        self.assertEqual(translate_a_sentence("a b", test_encoding), "0 1")
        self.assertEqual(translate_a_sentence("aa cc", test_encoding), "00 22")

if __name__ == '__main__':
    main()
