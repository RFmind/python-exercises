from unittest import TestCase, main
from solution import get_neighbor_positions, get_next_generation, next_board

class GameOfLifeTest(TestCase):

    def test_get_neighbors(self):
        board = [[1,2,3], [4,5,6], [7,8,9]]
        width = 3
        height = 3

        expected1 = [(0,1),(1,0),(1,1)]
        result1 = get_neighbor_positions(0,0,width,height)
        self.assertEqual(expected1, result1)

        expected2 = [(0,0),(0,1),(0,2),(1,0),(1,2),(2,0),(2,1),(2,2)]
        result2 = get_neighbot_positions(1,1,width,height)
        self.assertEqual(expected2, result2)

    def test_get_next_generation(self):
        board = [[1,2,3], [4,5,6], [7,8,9]]
        width = 3
        height = 3

        expected1 = True
        result1 = get_next_generation(True, [True,True,False])
        self.assertEqual(expected1, result1)

        expected2 = False
        result2 = get_next_generation(True, [False, False, True])
        self.assertEqual(expected2, result2)

        expected3 = False
        result3 = get_next_generation(False, [False, False, True])
        self.assertEqual(expected3, result3)

        expected4 = False
        result4 = get_next_generation(True, [True, True, True, True])
        self.assertEqual(expected4, result4)

        expected5 = False
        result5 = get_next_generation(False, [True, True, True, True])
        self.assertEqual(expected5, result5)

        expected6 = True
        result6 = get_next_generation(False, [True, True, True])
        self.assertEqual(expected6, result6)

        expected7 = True
        result7 = get_next_generation(True, [True, True])
        self.assertEqual(expected7, result7)

    def test_next_board(self):
        board1 = [
            [False, False, False],
            [False, False, False],
            [False, False, False]
        ]
        board2 = [
            [False, True, True],
            [False, True, True],
            [True, False, False]
        ]
        expected1 = [
            [False, False, False],
            [False, False, False],
            [False, False, False]
        ]
        expected2 = [
            [False, True, True],
            [True, False, True],
            [False, True, False]
        ]

        self.assertEqual(expected1, next_board(board1))
        self.assertEqual(expected2, next_board(board2))

if __name__ == '__main__':
    main()
