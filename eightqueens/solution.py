from copy import deepcopy

def eight_queens(board):
    '''
    given a NxN (square) empty board,
    return a board with exactly one queen on each row and each column
    
    the board is a list of lists where False means empty and True means queen.

    example board:
        [
            0 [False, False, False],
            1 [False, False, False],
            2 [False, False, False]
        ]
    '''

    # result[x][y] = True or False

    result = deepcopy(board)

    # fill the result with queens
    n = len(result)
    each_column = {column_index:False for column_index in range(n)}

        # Alternative code zoals line 24 om bij te houden of er al een queen is in de kolom
        # each_column = {}
        # for column_index in range(n):
        #     each_column[column_index] = False

    for i in range(n):
        # looping door het aantal rijen
        has_queen = False

        for j in range(n):
            # looping door het aantal kolommen in die rijen
            if has_queen == False and each_column[j] == False:
                result[i][j] = True
                each_column[j] = True
                has_queen = True

    return result
