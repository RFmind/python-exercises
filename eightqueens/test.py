from unittest import TestCase, main
from solution import eight_queens

class EightQueensTest(TestCase):

    def test_3x3_board(self):

        board = [ [False for j in range(3)] for i in range(3) ]
        result = eight_queens(board)

        rows_counts = { i: 0 for i in range(3) }
        cols_counts = { i: 0 for i in range(3) }
        count = 0

        for i in range(3):
            for j in range(3):
                if result[i][j]:
                    self.assertTrue(rows_counts[i] == 0)
                    self.assertTrue(cols_counts[j] == 0)
                    count += 1
                    rows_counts[i] += 1
                    cols_counts[j] += 1

        self.assertEqual(3, count)

if __name__ == '__main__':
    main()
